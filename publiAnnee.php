<?php
include_once 'includes/config.inc.php';
include_once 'includes/hal.class.php';
$_hal=new Hal($_config['hal']['headers']);
$_hal->setDebug($_config['hal']['debug']['print']);
$_hal->setError($_config['hal']['error']['print'], $_config['hal']['debug']['halt']);

$idHal=$_config['hal']['idHal'];
$collection=$_config['hal']['collection'];

if(!empty($idHal)){
	// Récupération des infos sur l'auteur
	$query=(is_numeric($idHal)?'idHal_i:':'idHal_s:').$idHal;
	$params=array(
		'q'	=> $query,
	);
	$auteurs=$_hal->getRef('author', $params);
	if(!empty($auteurs)){
		$auteur=$auteurs[0];

		// Récupération des publications de moins de 3 ans, triées par date
		$query=(is_numeric($idHal)?'authIdHal_i:':'authIdHal_s:').$idHal;
		$params=$_config['hal']['search'];
		$params=array(
			'q'	=> $query,
			'fl'    => 'title_s,authFullName_s,citationRef_s,docType_s,fileMain_s,producedDateY_i',
			'sort'  => 'producedDate_s desc',
		);
		$publications=$_hal->getPublications($params, $collection);
		$nbPublications=count($publications);

		// Récupération des types de document
		$params=array(
			'q'	=> '*',
		);
		$docTypes=$_hal->getRef('doctype', $params);
		$types=array();
		foreach((array)$docTypes as $docType){
			$types[$docType['str'][0]]=$docType['str'][1];
		}
		unset($docTypes);

		// Classement des publications par type de document
		$documents=array();
		foreach((array)$publications as $publication){
			$documents[$publication['producedDateY_i']][$publication['docType_s']][]=$publication;
		}
		unset($publications);
	}
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8"/>
<title>Publications de <?php echo $auteur['label_s'];?> classées par année et par type</title>
<style>
ul{
	padding: 0;
	margin: 1em;
	display: inline-block;
}
main span{
	display: block;
}
main span.title{
	font-weight: bold;
}
</style>
</head>
<body>
<?php
if(!empty($auteur)){
	?>
	<header>
	<h1>Publications de <?php echo $auteur['label_s'];?> classées par année et par type</h1>
	</header>
	<?php
	if(!empty($documents)){
		$annees=array_keys((array)$documents);
		?>
		<nav>
		<ul>
		<?php
		foreach((array)$annees as $annee){
			echo '<li><a href="#annee-'.$annee.'">'.$annee.'</a></li>';
		}
		?>
		</ul>
		</nav>
		<main>
		<?php
		foreach((array)$annees as $annee){
			echo '<div id="da-'.$annee.'">';
			echo '<h2 id="annee-'.$annee.'">'.$annee.'</h2>'.PHP_EOL;
			$publications=$documents[$annee];
			foreach((array)$types as $type=>$label){
				if(!empty($publications[$type])){
					echo '<div id="dt-'.$annee.'-'.$type.'">';
					echo '<h3 id="type-'.$annee.'-'.$type.'">'.$types[$type].'</h3>';
					foreach((array)$publications[$type] as $publication){
						echo '<p>';
						echo '<span class="authors">'.implode(', ',(array)$publication['authFullName_s']).'</span>';
						echo '<span class="title">'.$publication['title_s'][0].'</span>';
						echo '<span class="citation">'.$publication['citationRef_s'].'</span>';
						if(!empty($publication['fileMain_s'])){
							echo '<span class="file"><a href="'.$publication['fileMain_s'].'" target="_blank">'.$publication['fileMain_s'].'</a></span>';
						}
						echo '</p>';
					}
					echo '</div>';
				}
			}
			echo '</div>';
		}
		?>
		</main>
		<?php
	}
}
?>
</body>
</html>
