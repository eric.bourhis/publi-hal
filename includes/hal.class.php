<?php
class Hal{
	protected $echoDebug	= false;
	protected $haltOnError	= false;
	protected $echoError	= false;
	protected $debug	= array('date'=>'', 'fct'=>'', 'msg'=>'', 'info'=>'');
	protected $error	= array('date'=>'', 'fct'=>'', 'code'=>'', 'msg'=>'', 'info'=>'');
	protected $host		= '';
	protected $headers	= array();

	const ERR_PARAM		= 'ERR_PARAM';
	const ERR_PARAM_ID	= 'ERR_PARAM_ID';
	const ERR_NOT_EMPTY	= 'ERR_NOT_EMPTY';
	const ERR_PROTECT	= 'ERR_PROTECT';

	const HAL_ERR_HTTP	= 'HAL_ERR_HTTP';
	const HAL_ERR_CURL	= 'HAL_ERR_CURL';

	const HAL_HOST		= 'https://api.archives-ouvertes.fr/';
	const HAL_REF_FORMAT	= 'json';
	const HAL_REF_ROWS	= 1000;


	/* constructeur */
	public function __construct($headers=array()){
		$this->host = self::HAL_HOST;
		foreach((array)$headers as $key=>$value){
			if($value===''){
				continue;
			}
			$this->headers[]=$key.': '.$value;
		}
	}

	/* private */
	private function _search($ref, $params){
		if(empty($params) || !is_array($params)){
			self::_setErr(__FUNCTION__, self::ERR_PARAM, 'paramètres obligatoires', func_get_args());
			return false;
		}
		if(empty($params['q'])){
			self::_setErr(__FUNCTION__, self::ERR_PARAM, 'paramètre de recherche obligatoire', func_get_args());
			return false;
		}
		unset($params['wt']);
		unset($params['rows']);
		unset($params['start']);
		$queries=array();
		foreach($params as $param=>$value){
			if($value===''){
				continue;
			}
			$queries[]=urlencode($param).'='.urlencode($value);
		}
		$query=implode('&', $queries);

		$url=$this->host.$ref.'/';
		$url.='?'.$query;
		$url.='&wt='.self::HAL_REF_FORMAT;
		$url.='&rows='.self::HAL_REF_ROWS;

		$records=array();
		$start=0;
		do{
			$urlStart=$url.'&start='.$start;
			$json=self::_get($urlStart);
			$reponses=json_decode($json, 1);
			$results=(array)(isset($reponses['response']['docs'])?$reponses['response']['docs']:$reponses['response']['result']['doc']);
			$nbRecords=count($results);
			$records=array_merge($records, $results);
			$start+=$nbRecords;
		} while($nbRecords == self::HAL_REF_ROWS);
		return $records;
	}
	/* protected */
	protected function _setErr($fonction, $code, $msg, $info=''){
		$this->error['date']	= date('Y-m-d H:i:s');
		$this->error['fct']	= $fonction;
		$this->error['info']	= $info;
		$this->error['code']	= $code;
		$this->error['msg']	= $msg;
		self::echoError();
		if($this->haltOnError){
			die();
		}
	}

	protected function _setDebug($fonction, $msg, $info=''){
		$this->debug['date']	= date('Y-m-d H:i:s');
		$this->debug['fct']	= $fonction;
		$this->debug['info']	= $info;
		$this->debug['msg']	= $msg;
		self::echoDebug();
	}

	protected function _get($url, $headers=array()){
		self::_setDebug(__FUNCTION__, 'Requête : '.$url);
		$headers=(!empty($headers)?$headers:$this->headers);
		$ch = curl_init();
		$options = array(
			CURLOPT_RETURNTRANSFER	=> true,
			CURLOPT_URL		=> $url,
			CURLOPT_HTTPHEADER	=> $headers,
		);
		self::_setDebug(__FUNCTION__, 'Options CURL : '.print_r($options, 1));
		curl_setopt_array($ch, $options);
		$response=curl_exec($ch);
		if(curl_errno($ch)){
			self::_setErr(__FUNCTION__, self::HAL_ERR_HTTP, 'Erreur CURL : '.curl_error($ch).' ('.curl_errno($ch).')', func_get_args());
			return false;
		}
		$httpCode=curl_getinfo($ch, CURLINFO_HTTP_CODE);
		self::_setDebug(__FUNCTION__, 'Code réponse HTTP : '.$httpCode);
		if($httpCode!=200){
			self::_setErr(__FUNCTION__, self::HAL_ERR_HTTP, 'Erreur HTTP : '.$httpCode, func_get_args());
			return false;
		}
		curl_close($ch);
		self::_setDebug(__FUNCTION__, 'Réponse : '.$response);
		return $response;
	}

	/* public */
	// Debug
	// Activer/désactiver l'affichage des logs de debug
	public function setDebug($echoDebug){
		$this->echoDebug=(bool)$echoDebug;
	}
	// Récupérer la dernière entrée de debug
	public function getDebug(){
		return $this->debug;
	}
	// Afficher la dernière entrée de debug
	protected function echoDebug(){
		if($this->echoDebug){
			echo '[DEBUG]'."\t".$this->debug['date']."\t".$this->debug['fct'].' : '.$this->debug['msg'].PHP_EOL;
		}
	}

	// Erreurs
	// Activer/désactiver l'affichage des logs d'erreur et activer/désactiver l'arret de l'exécution en cas d'erreur
	public function setError($echoError=null, $haltOnError=null){
		$this->echoError	= (is_null($echoError)?$this->echoError:(bool)$echoError);
		$this->haltOnError	= (is_null($haltOnError)?$this->haltOnError:(bool)$haltOnError);
	}
	// Récupérer la dernière entrée des erreurs
	public function getError(){
		return $this->error;
	}
	// Afficher la dernière entrée des erreurs
	protected function echoError(){
		if($this->echoError){
			echo '[ERROR]'."\t".$this->error['date']."\t".$this->error['fct'].' : '.$this->error['msg'].'('.$this->error['code'].')'.PHP_EOL;
		}
	}

	// HAL
	// Interrogation des bases de référence
	public function getRef($ref, $params){
		return self::_search('ref/'.$ref, $params);
	}

	// Recherche des publications
	public function getPublications($params, $collection=''){
		$ref='search'.(!empty($collection)?'/'.$collection:'');
		return self::_search($ref, $params);
	}

	// Recherche des publications par idHal + sélection et des données retournées + tri des résultats
	public function getPublicationsByIdHal($idHal, $collection=''){
		if(empty($idHal)){
			self::_setErr(__FUNCTION__, self::ERR_PARAM, 'IdHal obligatoire', func_get_args());
			return false;
		}
		$query=(is_numeric($idHal)?'authIdHal_i:':'authIdHal_s:').$idHal;
		$params=array(
			'q'=>$query,
			'fl'=>'label_s,title_s,authFullName_s,citationRef_s,docType_s,fileMain_s,files_s',
			'sort'	=> 'producedDate_s desc',
		);
		return self::getPublications($params, $collection);
	}
}
