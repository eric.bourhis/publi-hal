<?php
$_config=array();

$_config['hal']['debug']['print']=false; // Afficher les messages de debug
$_config['hal']['error']['print']=false; // Afficher les messages d'erreur
$_config['hal']['error']['halt']=false; // Arrêter l'exécution du script en cas d'erreur

$_config['hal']['headers']=array(
	'From'	=> 'informatique@imtbs-tsp.eu',
);

$_config['hal']['idHal']='';

//$_config['hal']['collection']='TELECOM-SUDPARIS';
//$_config['hal']['collection']='TELECOM-MANAGEMENT';

if(file_exists(__DIR__.'/local.inc.php')){
	include_once __DIR__.'/local.inc.php';
}
?>
